var waypoint = new Waypoint({
    element: document.getElementById('header'),
    handler: function(element) {
        this.element.classList.add('waypoint');
    },
    offset: '50%'
});

var waypoint0 = new Waypoint({
    element: document.getElementById('banner-inicio'),
    handler: function(element) {
        this.element.classList.add('waypoint');
    },
    offset: '50%'
});

var waypoint1 = new Waypoint({
    element: document.getElementById('section-01'),
    handler: function(element) {
        this.element.classList.add('waypoint');
    },
    offset: '65%'
});

var waypoint2 = new Waypoint({
    element: document.getElementById('section-02'),
    handler: function(element) {
        this.element.classList.add('waypoint');
    },
    offset: '65%'
});

var waypoint3 = new Waypoint({
    element: document.getElementById('section-03'),
    handler: function(element) {
        this.element.classList.add('waypoint');
    },
    offset: '65%'
});




